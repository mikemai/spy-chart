class CandlestickChart {
    constructor(data, sliceNo, containerId, title) {
        this.data = data;
        this.sliceNo = sliceNo;
        this.containerId = containerId;
        this.title = title;
    }

    plotChart() {
    var candlestickTrace = {
            x: this.data.map(row => row['Date']),
            close: this.data.map(row => row['Close']),
            high: this.data.map(row => row['High']),
            low: this.data.map(row => row['Low']),
            open: this.data.map(row => row['Open']),
            type: 'candlestick',
            name: 'Candlestick'
        };

        var vwapTrace = {
            x: this.data.slice(this.sliceNo).map(row => row['Date']),
            y: this.data.slice(this.sliceNo).map(row => row['VWAP']),
            type: 'scatter',
            mode: 'lines',
            name: 'VWAP',
            line: {color: 'cornflowerblue'},
            xaxis: 'x',
            yaxis: 'y'
        };

        var smaTrace = {
            x: this.data.map(row => row['Date']),
            y: this.data.map(row => row['SMA']),
            type: 'scatter',
            mode: 'lines',
            name: 'Simple Moving Average',
            line: {color: 'orange'},
            xaxis: 'x',
            yaxis: 'y'
        };

        var rsiTrace = {
            x: this.data.map(row => row['Date']),
            y: this.data.map(row => row['RSI']),
            type: 'scatter',
            mode: 'lines',
            name: 'RSI',
            line: {color: 'red'},
            xaxis: 'x2',
            yaxis: 'y2'
        };

        var markerTrace = {
            x: this.data.map(row => row['Date']),
            y: this.data.map(row => row['consecutive_highs']),
            mode: 'markers',
            name: 'Signal',
            marker: {
                symbol: 'triangle-down',
                size: 20}
        };

        var layout = {
            title: this.title,
            xaxis: {
                domain: [0, 1],
                type: 'category',
                rangeslider: { visible: false },
                showticklabels: false,
                showspikes: true, spikemode: 'across', spikesnap: 'cursor', spikethickness: 1
            },
            yaxis: {
                domain: [0.2, 1],
                showspikes: true, spikemode: 'across', spikesnap: 'cursor', spikethickness: 1},
            xaxis2: {
                domain: [0, 1], anchor: 'y2', type: 'category', showticklabels: false,
                showspikes: true, spikemode: 'across', spikesnap: 'cursor', spikethickness: 1},
            yaxis2: {
                domain: [0, 0.2],
                showspikes: true,spikemode: 'across', spikesnap: 'cursor', spikethickness: 1},
            dragmode: 'pan',
            paper_bgcolor: 'rgba(40,40,40,1)',
            plot_bgcolor: 'rgba(40,40,40,1)',
            font: {
                color: 'white'
            }
        };

        Plotly.newPlot(this.containerId, [candlestickTrace, vwapTrace, smaTrace, rsiTrace, markerTrace], layout, {scrollZoom: true}).then(function(chart) {
            // Get the initial range of the primary x-axis
            var initialRange = chart.layout.xaxis.range;
        
            // Set the range of the secondary x-axis to match
            Plotly.relayout(chart, {
                'xaxis2.range[0]': initialRange[0],
                'xaxis2.range[1]': initialRange[1]
            });
        
            chart.on('plotly_relayout', function(eventData) {
                if (eventData['xaxis.range[0]'] && eventData['xaxis.range[1]']) {
                    Plotly.relayout(chart, {
                        'xaxis2.range[0]': eventData['xaxis.range[0]'],
                        'xaxis2.range[1]': eventData['xaxis.range[1]']
                    });
                }
            });
        });
    }
}

function toggleSidePanel() {
    var panel = document.getElementById("sidePanel");
    console.log(panel.style.width)
    if (panel.style.width === "0px" || panel.style.width === "") {
        panel.style.width = "400px";
        panel.style.paddingLeft = "20px";
        panel.style.paddingRight = "20px";
        document.addEventListener('click', closePanel);
    } else {
        panel.style.width = "0px";
        panel.style.paddingLeft = "0px";
        panel.style.paddingRight = "0px";
        document.removeEventListener('click', closePanel);
    }
}

function closePanel(e) {
    var toggleButton = document.querySelector('.header button:nth-child(2)');
    if (e.target !== toggleButton) { // Only close if the click is not on the toggle button
        var panel = document.getElementById("sidePanel");
        panel.style.width = "0px";
        panel.style.paddingLeft = "0px";
        panel.style.paddingRight = "0px";
        document.removeEventListener('click', closePanel);
    }
}

function fetchData_1(value) {
    fetch('/get_data_1?value=' + value)
        .then(response => response.json())
        .then(data1 => {
            var chart1 = new CandlestickChart(data1, 0, 'chart-container-1', 'Chart Title 1');
            chart1.plotChart();
        })
        .catch(error => console.error('Error:', error));
}

function fetchData_2(value) {
    fetch('/get_data_2?value=' + value)
        .then(response => response.json())
        .then(data2 => {
            var chart2 = new CandlestickChart(data2, 0, 'chart-container-2', 'Chart Title 2');
            chart2.plotChart();
        })
        .catch(error => console.error('Error:', error));
}
