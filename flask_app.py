from flask import Flask, render_template, jsonify, request
from datetime import datetime
import shared_data
import pandas as pd
import numpy as np
import os

def run_flask_app():
    app = Flask(__name__)

    def read_csv_1(mode):
        if mode == 'backtest_mode':
            file_path = 'data/filtered_data.csv'
            file_path2 = 'data/backtest_trigger_history.csv'

            df = pd.read_csv(file_path)
            df2 = pd.read_csv(file_path2)

            # Merge the dataframes as needed. Modify the 'on' parameter based on the common column(s)
            merged_df = pd.merge(df, df2, on='Date', how='outer')
            merged_df.fillna(value='', inplace=True)

            return merged_df.to_dict(orient='records')
        else:
            file_path = 'data/spy_5min_data.csv'
            file_path2 = 'data/trigger_history.csv'

            df = pd.read_csv(file_path)
            df2 = pd.read_csv(file_path2)

            # Merge the dataframes as needed. Modify the 'on' parameter based on the common column(s)
            merged_df = pd.merge(df, df2, on='Date', how='outer')
            merged_df.fillna(value='', inplace=True)

            # Process the data as needed
            return merged_df.to_dict(orient='records')

    def read_csv_2():
        file_path = 'data/spy_15min_data.csv'
        df = pd.read_csv(file_path)
        # Process the data as needed
        return df.to_dict(orient='records')

    def generate_popup_message(message):
        return f"""
            <html>
                <body>
                    <script>
                        alert("{message}");
                        window.location.href = "/";
                    </script>
                </body>
            </html>
        """

    @app.route('/')
    def index():
        side_panel_content = '''very long very long very long very long very long very long very long 
                                <br><br>very long very long very long very long content for the side panel.'''
        return render_template('index.html', content=side_panel_content)

    @app.route('/strategies_builder')
    def strategies_builder():
        strategies_path = 'data/strategies.csv'

        containers = [
            {'header': 'Main Setting', 'strategy_value': 'strategy1', 'parameter_value': '3'},
            {'header': 'Consecutive Highs', 'strategy_value': 'strategy1', 'parameter_value': '3'},
            {'header': 'Header 3', 'strategy_value': 'strategy3', 'parameter_value': 'param3'},
            {'header': 'Header 4', 'strategy_value': 'strategy4', 'parameter_value': 'param4'},
            {'header': 'Header 5', 'strategy_value': 'strategy5', 'parameter_value': 'param5'}
        ]

        df = pd.read_csv(strategies_path)
        for container in containers:
            header_value = container['header']
            row = df[df['header'] == header_value]
            if not row.empty:
                container['checkbox_checked'] = 'checked' if row['checkbox'].iloc[0] == 'checked' else ''
            else:
                container['checkbox_checked'] = ''

        return render_template('strategies_builder.html', containers=containers)

    @app.route('/save_strategy/<int:strategy_number>', methods=['POST'])
    def save_strategy(strategy_number):
        # Retrieve the form data
        header_value = request.form['header']
        strategy_value = request.form['strategy']
        parameter_value = request.form['parameter']
        checkbox_value = 'checked' if 'checkbox_name' in request.form else 'unchecked'

        # Define the CSV file path
        csv_file_path = 'data/strategies.csv'

        # Read the existing CSV file into a DataFrame (create an empty DataFrame if the file doesn't exist)
        if os.path.exists(csv_file_path):
            df = pd.read_csv(csv_file_path)
        else:
            df = pd.DataFrame(columns=['header', 'strategy_value', 'parameter_value', 'checkbox'])

        # Search for the row with the matching header
        row_index = df[df['header'] == header_value].index

        # Update the row values if found, or add a new row if not found
        row_data = {'header': header_value, 'strategy_value': strategy_value, 'parameter_value': parameter_value, 'checkbox': checkbox_value}
        if row_index.empty:
            new_row = pd.DataFrame([row_data])  # Create a DataFrame for the new row
            df = pd.concat([df, new_row], ignore_index=True)  # Concatenate the existing DataFrame with the new row
        else:
            df.loc[row_index[0]] = row_data

        # Save the DataFrame back to the CSV file
        df.to_csv(csv_file_path, index=False)

        return generate_popup_message('Data saved successfully!')

    @app.route('/backtest', methods=['GET', 'POST'])
    def backtest():
        side_panel_content='test text'
        if request.method == 'POST':
            start_date = datetime.strptime(request.form['start_date'], '%Y-%m-%d')
            end_date = datetime.strptime(request.form['end_date'], '%Y-%m-%d')

            shared_data.write_data('backtest_start_date', start_date)
            shared_data.write_data('backtest_end_date', end_date)
        return render_template('backtest.html', content=side_panel_content)

    @app.route('/get_data_1')
    def get_data_1():
        value = request.args.get('value')
        data = read_csv_1(value)
        return jsonify(data)

    @app.route('/get_data_2')
    def get_data_2():
        value = request.args.get('value')
        data = read_csv_2()
        return jsonify(data)

    app.run(debug=False)
