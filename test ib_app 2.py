from ib_insync import *
ib = IB()
ib.connect('127.0.0.1', 7497, clientId=10)
ib.reqMarketDataType(1)
 
option1 = Option(
    symbol = 'SPX',
    lastTradeDateOrContractMonth = 20230816,
    strike = 4420,
    exchange = 'SMART',
    multiplier = 100,
    currency ='USD',
    right = 'PUT'
)
 
option2 = Option(
    symbol = 'SPX',
    lastTradeDateOrContractMonth = 20230816,
    strike = 4420,
    exchange = 'SMART',
    multiplier = 100,
    currency ='USD',
    right = 'CALL'
)
 
option1, option2 = ib.qualifyContracts(option1, option2)
print(option1.conId)
print(option2.conId)
 
contract = Contract(symbol='SPX', secType='BAG', exchange='SMART', currency='USD', comboLegs = [
    ComboLeg(option1.conId, ratio=1, action='BUY'),
    ComboLeg(option2.conId, ratio=1, action='BUY')
])

order = MarketOrder('BUY', 1, account=ib.wrapper.accounts[0])

trade = ib.placeOrder(contract, order)
ib.sleep(5)
print(trade)
 
ib.disconnect()

'''
# working code to place option order
contract = Contract(symbol='SPX', secType='IND', exchange='SMART', currency='USD')
ib.qualifyContracts(contract)
options_contract = Option(contract.symbol, 20230811, 4450, 'C', 'SMART')
options_order = MarketOrder('BUY', 1,account=ib.wrapper.accounts[0])
trade = ib.placeOrder(options_contract, options_order)
'''