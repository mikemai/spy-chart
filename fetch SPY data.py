import yfinance as yf
import pandas as pd

# Define the ticker symbol
ticker_symbol = 'SPY'

# Get data on the specified ticker symbol
ticker_data = yf.Ticker(ticker_symbol)

# Get historical data (7 days, 1 minute intervals)
new_data = ticker_data.history(period='7d', interval='1m')

# Read existing data from CSV
csv_path = 'data/SPY_1min_Main.csv'
existing_data = pd.read_csv(csv_path, index_col=0, parse_dates=True)

# Merge the existing data with the new data, keeping the latest entries
combined_data = new_data.combine_first(existing_data)

# Save the data to the same CSV file
combined_data.to_csv(csv_path)

print('Data appended to SPY_1min_Main.csv without overwriting or duplicates.')
