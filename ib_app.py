from flask_app import run_flask_app
from shared_data import data, data_lock
from ib_insync import *
from simulated_streaming import *
from indicators import *
from trigger_condition import *
import threading
import pandas as pd

# Set to False to use simulated data
use_real_time_bars = False

flask_thread = threading.Thread(target=run_flask_app)
flask_thread.start()

# Create DataFrames
prev_day_5min = pd.read_csv('data/prev_spy_5min_data.csv')
prev_day_5min['Date'] = pd.to_datetime(prev_day_5min['Date'])
prev_2day_15min = pd.read_csv('data/prev_spy_15min_data.csv')
prev_2day_15min['Date'] = pd.to_datetime(prev_2day_15min['Date'])
df_5_sec = pd.DataFrame(columns=['Date', 'Open', 'High', 'Low', 'Close', 'Volume'])

class Resampler:
    def __init__(self, df):
        self.df = df
        self.ohlcv_dict = {
            'Date': 'last',
            'Open': 'first',
            'High': 'max',
            'Low': 'min',
            'Close': 'last',
            'Volume': 'sum'
        }

    def resample(self, interval, prev_data=pd.DataFrame()):
        df_resampled = self.df.resample(interval, on='Date').apply(self.ohlcv_dict)
        df_resampled.dropna(inplace=True)
        
        # Check if prev_data is empty, and concatenate if not
        if prev_data.empty:
            new_df = df_resampled
        else:
            new_df = pd.concat([prev_data, df_resampled], ignore_index=True)

        new_df['VWAP'] = vwap(new_df)
        new_df['SMA'] = sma(new_df)
        new_df['RSI'] = rsi(new_df)
        new_df = new_df[['Date', 'Open', 'High', 'Low', 'Close', 'Volume', 'VWAP', 'SMA', 'RSI']]
        return new_df

    def save_to_csv(self, df, filename):
        with open(filename, 'w', newline='') as file:
            df.to_csv(file, index=False)

def executableEvery5Min(df_5_sec):
    resampler = Resampler(df_5_sec)

    df_5_min = resampler.resample('5T', prev_day_5min)
    resampler.save_to_csv(df_5_min, 'data/spy_5min_data.csv')

    df_15_min = resampler.resample('15T', prev_2day_15min)
    resampler.save_to_csv(df_15_min, 'data/spy_15min_data.csv')

    return df_5_min, df_15_min

def on_bar_update(bars, has_new_bar):
    global df_5_sec
    bar = bars[-1]
    # print('current price is ' + str(bar.open_))
    # print(bar)

    new_row = pd.DataFrame({
        'Date': bar.time,
        'Open': bar.open_,
        'High': bar.high,
        'Low': bar.low,
        'Close': bar.close,
        'Volume': bar.volume
    }, index=[0])

    df_5_sec = pd.concat([df_5_sec, new_row], ignore_index=True)

    # Resample every 5 minutes and save to CSV
    if bar.time.minute % 5 == 0 and bar.time.second == 0:
        print('bar update 5min')
        df_5_min, df_15_min = executableEvery5Min(df_5_sec)
        if check_consecutive_highs(df_5_min, mode='live'):
            print('trigger !!!!!')

if use_real_time_bars:
    ib = IB()
    ib.connect('127.0.0.1', 7497, clientId=1)

    # Define the contract
    contract = Contract(symbol='SPY', secType='STK', exchange='SMART', currency='USD')
    ib.qualifyContracts(contract)

    # Subscribe to 5-second real-time bars
    bars = ib.reqRealTimeBars(contract, 5, 'TRADES', False)
    bars.updateEvent += on_bar_update
else:
    csv_path = 'data/SPY_1min.csv'
    simulator = SimulatedRealTimeStreaming(csv_path)
    for simulated_bars in simulator.stream():
        on_bar_update(simulated_bars, True)

try:
    ib.run()
except KeyboardInterrupt:
    print("\nExiting...")

bars.updateEvent -= on_bar_update
ib.cancelRealTimeBars(bars)
ib.disconnect()