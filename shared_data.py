import threading

# Shared global variables with multiple data points
data = {
    'backtest_start_date': None,
    'backtest_end_date': None,
    'user_activity': None
}
data_lock = threading.Lock()

def write_data(key, value):
    with data_lock:
        data[key] = value

def read_data(key):
    with data_lock:
        value = data.get(key)
    return value