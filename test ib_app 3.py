from ib_insync import *
import datetime
import numpy as np
import time

#Required if you run it through an IDE
#util.startLoop()  

ib = IB()
ib.connect('127.0.0.1', 7497, clientId=1)
    
#Obtain current spy price and round it to nearest integer
spy_contract = Stock('SPY', 'SMART', 'USD')
                                       
bars_spy = ib.reqHistoricalData(
        spy_contract,
        endDateTime='',
        durationStr='60 s',
        barSizeSetting='1 secs',
        whatToShow='MIDPOINT',
        useRTH=True,
        formatDate=1)

df_spy = util.df(bars_spy)                                              
avg_price = round(np.nanmean(df_spy.close))

#Obtain strike date
strike_date = (datetime.date.today() + datetime.timedelta(days=2)).strftime("%Y%m%d")

spy_call = Option('SPY', strike_date, avg_price, 'C', 'SMART')
ib.qualifyContracts(spy_call)

spy_put = Option('SPY', strike_date, avg_price, 'P', 'SMART')
ib.qualifyContracts(spy_put)

#Create a straddle contract
contract = Contract()
contract.symbol = spy_put.symbol
contract.secType = "BAG"
contract.currency = spy_put.currency
contract.exchange = spy_put.exchange

leg1 = ComboLeg()
leg1.conId = spy_call.conId 
leg1.ratio = 1
leg1.action = "SELL"
leg1.exchange = spy_put.exchange

leg2 = ComboLeg()
leg2.conId = spy_put.conId  #DBK MAR 15 2019 C
leg2.ratio = 1
leg2.action = "SELL"
leg2.exchange = spy_call.exchange

contract.comboLegs = []
contract.comboLegs.append(leg1)
contract.comboLegs.append(leg2)

#Obtain straddle price
bars = ib.reqHistoricalData(
        contract,
        endDateTime='',
        durationStr='60 s',
        barSizeSetting='1 secs',
        whatToShow='TRADES',
        useRTH=True,
        formatDate=1)
   
df = util.df(bars)                                              
avg_price = np.nanmean(df.close)

#Place a straddle order
order = LimitOrder("BUY", 10, round(avg_price*0.995,2), tif="DAY", account = ib.wrapper.accounts[0]) ##algos only DAY         
trade = ib.placeOrder(contract, order)     
print(trade)

time.sleep(5)
    
#Disconnect
ib.disconnect()